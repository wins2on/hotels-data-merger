# Hotel Data Merger

A tool which consolidates data from different sources and provides API to retrieve the standardized data.

### Description
There are two main parts of the data merger. First, before merging the data it must be brought to the same format. Current implementation takes a simple rule based approach to achieve this. For example, `id` of the sanitized hotel's json object becomes an attribute which contains string "id" but does not contain string "destination". `facilities` attribute gets converted to `amenities` and `details` gets converted to `description`. `latitude`, `longitude`, `address`, `city` and `country` all get transferred to a single `location` attribute. Strings which contain camel case words are converted to a space separated word sequence. `image` objects are brought to the same format and all contain `link` and `description` attributes.

Second, all sanitized data objects, which both `id` and `destination_id` attributes correspond, get merged. Merging is performed on the same attributes. If these attributes are lists, they get transformed into one list without duplicates. If attributes are dictionaries, they get merged into a single dictionary by merging same attributes within the dictionaries and maintaining original attributes present only in one of the dictionaries to be merged. When merging attributes which are strings, merged attribute becomes a longer of two strings. Motivation behind this solution is that longer strings contain more (potentially useful) information.

After sanitization and merging, attributes of hotels' data objects will appear in random order. Although they could potentially be sorted to have a particular order of attributes, it was decided that order of attributes will not matter since humans will not interact with this data.

### Optimizations
Data procurement happens only once when the system fetches data from the data providers. The retrieved data gets cached and when a user performs the get request to fetch certain hotels by `id` and `destination_id`, the system filters results from the cache. It saves time since the user does not have to query data providers each time he/she wants to find hotels. However, the data in the cache may become outdated. To solve this problem schedulers could be implemented which would update the cache every n-minutes. Schedulers' implementation is left for future work.

Data delivery is optimized by caching results of a particular query. Every next time the user submits the same query, the result will be retrieved from the cache, without performing expensive filtering operation.

### Setup

The tool requires Python 3 as well as `flask` and `requests` packages. To install them, please run the following commands:

```
pip install Flask
pip install requests
```


## Running application

##### In order to start the application, run

```
python src/app.py
```
Application will start on `localhost:5000` and its API endpoints will be available at `localhost:5000/api/`.

##### In order to get the data from data providers, go to your browser and input the following http request:

```
http://localhost:5000/api/update?urls=[comma separated list of data providers]
```
As an example, using data providers given in exercise description,
```
http://localhost:5000/api/update?urls=https://api.myjson.com/bins/gdmqa,https://api.myjson.com/bins/1fva3m,https://api.myjson.com/bins/j6kzm
```
This will load and preprocess (sanitize and merge) the data into memory.

##### In order to retrieve all hotels matching either a particular ID or destinationID, input the following http request to your browser:

```
http://localhost:5000/api/get?hotels=[comma separated list of hotel ids]&destination=[comma separated list of destination ids]
```
As an example:
```
http://localhost:5000/api/get?hotels=iJhz&destination=5432
```

## Running unit tests
From root directory, run:
```
python -m unittest
```
