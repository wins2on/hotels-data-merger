from flask import Flask, jsonify, request
from data_retriever import DataRetriever

app = Flask(__name__)

data_retriever = DataRetriever()
cache = dict()


def get_ids(mapping, hotels_ids):
    hotels_set = set()
    for hotels_id in hotels_ids:
        if hotels_id in mapping:
            for i in mapping[hotels_id]:
                hotels_set.add(i)
    return hotels_set


@app.route('/api/update', methods=['GET'])
def update():
    args = request.args
    if 'urls' in args:
        data_retriever.retrieve_data(args['urls'].split(','))
        cache.clear()
        return 'Data successfully loaded'
    else:
        return "No suppliers are indicated", 200


@app.route('/api/get', methods=['GET'])
def get():
    args = request.args
    hotels_to_retrieve = set()

    cache_key = ''
    for arg in args:
        cache_key += args[arg]

    if cache_key in cache:
        hotels_to_retrieve = cache[cache_key]
    else:
        if 'hotels' in args:
            hotels_to_retrieve = get_ids(data_retriever.get_id_map(), args['hotels'].split(','))
        if 'destination' in args:
            hotels_by_destination = get_ids(data_retriever.get_destination_map(), args['destination'].split(','))
            if len(hotels_to_retrieve) == 0:
                hotels_to_retrieve = hotels_by_destination
            else:
                hotels_to_retrieve = hotels_to_retrieve.intersection(hotels_by_destination)
        cache[cache_key] = hotels_to_retrieve

    result = list()
    all_hotels = data_retriever.get_merged_objects()
    for hotel_to_retrieve in hotels_to_retrieve:
        result.append(all_hotels[hotel_to_retrieve])
    return jsonify(result)


if __name__ == '__main__':
    app.run(debug=True)
