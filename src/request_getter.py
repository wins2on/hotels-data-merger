import requests


class GetRequestHandler:
    def __init__(self):
        self.data = []

    def get_from_server(self, url):
        response = requests.get(url)
        if response.status_code == 200:
            retrieved_json_array = response.json()
            if retrieved_json_array:
                for json_object in retrieved_json_array:
                    self.data.append(json_object)
        else:
            print('Server at ' + url + ' was not found')

    def get_data(self):
        return self.data
