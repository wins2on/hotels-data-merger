class Merger:
    def __init__(self):
        self.data = []

    def merge_lists(self, list1, list2):
        duplicate_set = set(list1)
        merged_lists = list1
        for l in list2:
            if l not in duplicate_set:
                merged_lists.append(l)
        return merged_lists

    def merge_dicts(self, dict1, dict2):
        merged_dict = dict1
        for field in dict2:
            if field in merged_dict:
                if merged_dict[field] != dict2[field] and len(str(merged_dict[field])) < len(str(dict2[field])):
                    merged_dict[field] = dict2[field]
            else:
                merged_dict[field] = dict2[field]
        return merged_dict

    def merge_amenities_all_with_type(self, dict_of_dicts_all, dict_of_dicts_type):
        all_set = set(dict_of_dicts_all['all'])
        for amenities_type in dict_of_dicts_type.keys():
            for amenity in dict_of_dicts_type[amenities_type]:
                if amenity in all_set:
                    dict_of_dicts_type[amenities_type] = self.merge_lists(dict_of_dicts_type[amenities_type], dict_of_dicts_all['all'])
                    return dict_of_dicts_type
        if 'general' in dict_of_dicts_type:
            dict_of_dicts_type['general'] = self.merge_lists(dict_of_dicts_type['general'], dict_of_dicts_all['all'])
        elif 'room' in dict_of_dicts_type:
            dict_of_dicts_type['room'] = self.merge_lists(dict_of_dicts_type['room'], dict_of_dicts_all['all'])
        return dict_of_dicts_type

    def merge_amenities_type_with_type(self, dict_of_dicts1, dict_of_dicts2):
        merged = dict_of_dicts1
        for amenity_type in dict_of_dicts2.keys():
            if amenity_type in merged:
                merged[amenity_type] = self.merge_lists(merged[amenity_type], dict_of_dicts2[amenity_type])
            else:
                merged[amenity_type] = dict_of_dicts2[amenity_type]
        return merged

    def merge_amenities(self, dict_of_dicts1, dict_of_dicts2):
        if 'all' in dict_of_dicts1 and 'all' in dict_of_dicts2:
            return {'all': self.merge_lists(dict_of_dicts1['all'], dict_of_dicts2['all'])}
        elif 'all' in dict_of_dicts1 and 'all' not in dict_of_dicts2:
            return self.merge_amenities_all_with_type(dict_of_dicts1, dict_of_dicts2)
        elif 'all' not in dict_of_dicts1 and 'all' in dict_of_dicts2:
            return self.merge_amenities_all_with_type(dict_of_dicts2, dict_of_dicts1)
        else:
            return self.merge_amenities_type_with_type(dict_of_dicts1, dict_of_dicts2)

    def merge_lists_of_images(self, list1, list2):
        links = set()
        for l in list1:
            links.add(l['link'])
        for l in list2:
            if l['link'] not in links:
                list1.append(l)
        return list1

    def merge_images(self, dict_of_dicts1, dict_of_dicts2):
        merged = dict_of_dicts1
        for image_type in dict_of_dicts2:
            if image_type in merged:
                merged[image_type] = self.merge_lists_of_images(dict_of_dicts1[image_type], dict_of_dicts2[image_type])
            else:
                merged[image_type] = dict_of_dicts2[image_type]
        return merged

    def merge_two_objs(self, obj1, obj2):
        merged_obj = obj1
        for field in obj2:
            if field in merged_obj:
                if field == 'location':
                    merged_obj['location'] = self.merge_dicts(obj1['location'], obj2['location'])
                elif field == 'amenities':
                    merged_obj['amenities'] = self.merge_amenities(obj1['amenities'], obj2['amenities'])
                elif field == 'images':
                    merged_obj['images'] = self.merge_images(obj1['images'], obj2['images'])
                elif field == 'booking_conditions':
                    merged_obj['booking_conditions'] = self.merge_lists(obj1['booking_conditions'], obj2['booking_conditions'])
                elif isinstance(merged_obj[field], str):
                    if merged_obj[field] != obj2[field] and len(str(obj2[field])) > len(str(merged_obj[field])):
                        merged_obj[field] = obj2[field]
            else:
                merged_obj[field] = obj2[field]
        return merged_obj

    def merge(self, data):
        merged_objects = dict()
        for obj in data:
            obj_id = str(obj['id']) + '___' + str(obj['destination_id'])
            if obj_id in merged_objects:
                merged_objects[obj_id] = self.merge_two_objs(merged_objects[obj_id], obj)
            else:
                merged_objects[obj_id] = obj
        id_map = dict()
        destination_map = dict()
        for obj_key in merged_objects.keys():
            id_key, destination_key = obj_key.split('___')
            if id_key in id_map:
                id_map[id_key].add(obj_key)
            else:
                id_map[id_key] = {obj_key}

            if destination_key in destination_map:
                destination_map[destination_key].add(obj_key)
            else:
                destination_map[destination_key] = {obj_key}
        return merged_objects, id_map, destination_map
