from request_getter import GetRequestHandler
from sanitizer import Sanitizer
from merger import Merger


class DataRetriever:
    def __init__(self):
        self.merged_objects = list()
        self.id_map = dict()
        self.destination_map = dict()

    def get_data_from_supplier(self, urls):
        get_request_handler = GetRequestHandler()
        for url in urls:
            get_request_handler.get_from_server(url)
        return get_request_handler.get_data()

    def sanitize_data(self, dirty_hotels_data):
        sanitizer = Sanitizer()
        return sanitizer.sanitize(dirty_hotels_data)

    def merge_data(self, sanitized_hotels_data):
        merger = Merger()
        return merger.merge(sanitized_hotels_data)

    def retrieve_data(self, urls):
        hotels_data = self.get_data_from_supplier(urls)

        self.sanitize_data(hotels_data)
        self.merged_objects, self.id_map, self.destination_map = self.merge_data(hotels_data)

    def get_merged_objects(self):
        return self.merged_objects

    def get_id_map(self):
        return self.id_map

    def get_destination_map(self):
        return self.destination_map
