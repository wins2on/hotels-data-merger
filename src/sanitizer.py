import re


class Sanitizer:
    def __init__(self):
        self.sanitized_data = []

    def split_camel_case(self, s):
        splitted_item = re.sub('([A-Z][a-z]+)', r' \1', re.sub('([A-Z]+)', r' \1', s.strip())).split()
        if len(splitted_item) > 1:
            result = ' '.join([i.lower() for i in splitted_item])
            if result == 'wi fi':
                return 'wifi'
            else:
                return result
        elif len(splitted_item) == 1:
            return splitted_item[0].lower()
        else:
            return ''

    def process_location_piece(self, location_piece, key, data):
        if 'location' not in data:
            data['location'] = {}
        removed_field = data.pop(key)
        if not removed_field:
            return

        if location_piece == 'postal':
            re_pattern = re.compile(removed_field)
            if 'address' in data['location']:
                if not re.search(re_pattern, data['location']['address']):
                    data['location']['address'] += ', ' + removed_field.strip()
            else:
                data['location']['address'] = removed_field.strip()
        elif location_piece == 'address':
            if 'address' in data['location']:
                data['location']['address'] = removed_field.strip() + ', ' + data['location']['address']
            else:
                data['location']['address'] = removed_field.strip()
        elif location_piece == 'lat' or location_piece == 'lng':
            data['location'][location_piece] = removed_field
        else:
            data['location'][location_piece] = removed_field.strip()


    def process_amenities_helper(self, key, data):
        processed_list = list()
        for item in data[key]:
            processed_item = self.split_camel_case(item)
            if processed_item:
                processed_list.append(processed_item)
        return processed_list

    def process_amenities(self, key, data):
        amenities = dict()
        if isinstance(data[key], dict):
            for type_key in data[key].keys():
                amenities[type_key.lower()] = self.process_amenities_helper(type_key, data[key])
        elif isinstance(data[key], list):
            amenities['all'] = self.process_amenities_helper(key, data)
        del data[key]
        data['amenities'] = amenities

    def process_images(self, key, data):
        if data[key]:
            for image_type in data[key].keys():
                for image in data[key][image_type]:
                    for data_field in image.keys():
                        if re.search(r'link$', data_field.lower()) or re.search(r'url$', data_field.lower()):
                            image['link'] = image.pop(data_field).strip()
                        elif re.search(r'caption$', data_field.lower()) or re.search(r'description$', data_field.lower()):
                            image['description'] = image.pop(data_field).strip()

    def sanitize(self, dirty_data):
        for entry in dirty_data:
            for key in list(entry.keys()):
                if re.search(r'(^id)|(id$)', key.lower()) and not re.search(r'destination', key.lower()):
                    entry['id'] = entry.pop(key)
                elif re.search(r'destination', key.lower()):
                    entry['destination_id'] = entry.pop(key)
                elif re.search(r'name$', key.lower()):
                    entry['name'] = entry.pop(key).strip()
                elif re.search(r'latitude$', key.lower()) or re.search(r'lat$', key.lower()):
                    self.process_location_piece('lat', key, entry)
                elif re.search(r'longitude$', key.lower()) or re.search(r'lng$', key.lower()):
                    self.process_location_piece('lng', key, entry)
                elif re.search(r'address$', key.lower()):
                    self.process_location_piece('address', key, entry)
                elif re.search(r'city$', key.lower()):
                    self.process_location_piece('city', key, entry)
                elif re.search(r'country$', key.lower()):
                    self.process_location_piece('country', key, entry)
                elif re.search(r'postal', key.lower()):
                    self.process_location_piece('postal', key, entry)
                elif re.search(r'description$', key.lower()) \
                        or re.search(r'info$', key.lower()) \
                        or re.search(r'details$', key.lower()):
                    removed_description = entry.pop(key)
                    if removed_description:
                        entry['description'] = removed_description.strip()
                elif re.search(r'amenities$', key.lower()) or re.search(r'facilities$', key.lower()):
                    self.process_amenities(key, entry)
                elif re.search(r'images$', key.lower()):
                    self.process_images(key, entry)
