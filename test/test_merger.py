import unittest
from src.merger import Merger


class SanitizerTest(unittest.TestCase):

    def setUp(self):
        self.merger = Merger()

    def test_merge_lists(self):
        l = self.merger.merge_lists(['a', 'b'], ['c'])
        self.assertListEqual(l, ['a', 'b', 'c'])

    def test_merge_dicts_all_unique(self):
        d = self.merger.merge_dicts({'1': 'a', '2': 'b'}, {'3': 'c'})
        self.assertDictEqual(d, {'1': 'a', '2': 'b', '3': 'c'})

    def test_merge_dicts_conflict(self):
        d = self.merger.merge_dicts({'1': 'a', '2': 'b'}, {'2': 'cc'})
        self.assertDictEqual(d, {'1': 'a', '2': 'cc'})

    def test_merge_amenities_all_with_type(self):
        all = {"all": ["tv", "coffee machine"]}
        with_type = {"general": ["business center"], "room": ["aircon", "coffee machine"]}
        d = self.merger.merge_amenities_all_with_type(all, with_type)
        self.assertDictEqual(d, {"general": ["business center"], "room": ["aircon", "coffee machine", "tv"]})

    def test_merge_amenities_type_with_type(self):
        type1 = {"general": ["pool"], "room": ["aircon", "coffee machine"]}
        type2 = {"general": ["business center"], "room": ["aircon", "coffee machine"]}
        d = self.merger.merge_amenities_type_with_type(type1, type2)
        self.assertDictEqual(d, {"general": ["pool", "business center"], "room": ["aircon", "coffee machine"]})

    def test_merge_amenities_two_alls(self):
        all1 = {"all": ["tv", "coffee machine"]}
        all2 = {"all": ["aircon", "coffee machine"]}
        d = self.merger.merge_amenities_all_with_type(all1, all2)
        self.assertDictEqual(d, {"all": ["aircon", "coffee machine", "tv"]})

    def test_merge_amenities_one_all_one_with_type(self):
        all = {"all": ["tv", "coffee machine"]}
        with_type = {"general": ["business center"], "room": ["aircon", "coffee machine"]}
        d = self.merger.merge_amenities_all_with_type(all, with_type)
        self.assertDictEqual(d, {"general": ["business center"], "room": ["aircon", "coffee machine", "tv"]})

    def test_merge_lists_of_images(self):
        list1 = [{"link": "https://s.cl.net/2.jpg", "description": "Double room"},
                 {"link": "https://d.t.net/3.jpg", "description": "Double room"}]
        list2 = [{"link": "https://s.cl.net/4.jpg", "description": "Single room"}]
        returned = self.merger.merge_lists_of_images(list1, list2)
        expected = [{"link": "https://s.cl.net/2.jpg", "description": "Double room"},
                    {"link": "https://d.t.net/3.jpg", "description": "Double room"},
                    {"link": "https://s.cl.net/4.jpg", "description": "Single room"}]
        self.assertListEqual(returned, expected)

    def test_merge_images(self):
        images1 = {
            "rooms": [
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/2.jpg", "description": "Double room"},
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/3.jpg", "description": "Double room"},
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/4.jpg", "description": "Bathroom"}],
            "site": [{"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/1.jpg", "description": "Front"}]
        }
        images2 = {
            "rooms": [{"link": "https://d2dfs.clount.net/0qZF/20.jpg", "description": "Single room"}],
            "site": [{"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/1.jpg", "description": "Front"}],
            "amenities": [{"link": "https://d2ey9s.net/0qZF/1.jpg", "description": "AC"}]
        }
        returned = self.merger.merge_images(images1, images2)
        expected = {
            "rooms": [
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/2.jpg", "description": "Double room"},
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/3.jpg", "description": "Double room"},
                {"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/4.jpg", "description": "Bathroom"},
                {"link": "https://d2dfs.clount.net/0qZF/20.jpg", "description": "Single room"}],
            "site": [{"link": "https://d2ey9sqrvkqdfs.cloudfront.net/0qZF/1.jpg", "description": "Front"}],
            "amenities": [{"link": "https://d2ey9s.net/0qZF/1.jpg", "description": "AC"}]
        }
        self.assertDictEqual(returned, expected)
