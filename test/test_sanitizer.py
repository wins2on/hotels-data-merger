import unittest
from src.sanitizer import Sanitizer


class SanitizerTest(unittest.TestCase):

    def setUp(self):
        self.sanitizer = Sanitizer()

    def test_split_camel_case(self):
        self.assertEqual(self.sanitizer.split_camel_case('camelCase'), 'camel case')
        self.assertEqual(self.sanitizer.split_camel_case('Notcamelcase'), 'notcamelcase')
        self.assertEqual(self.sanitizer.split_camel_case('WiFi'), 'wifi')
        self.assertEqual(self.sanitizer.split_camel_case('mainBusinessCentre'), 'main business centre')

    def test_process_location_piece_null(self):
        data = {'key': None}
        location_piece = 'address'
        self.sanitizer.process_location_piece(location_piece, 'key', data)
        self.assertDictEqual({'location': {}}, data)

    def test_process_location_piece_postal1(self):
        data = {'postalCode': 'ABC123'}
        location_piece = 'postal'
        self.sanitizer.process_location_piece(location_piece, 'postalCode', data)
        self.assertDictEqual({'location': {'address': 'ABC123'}}, data)

    def test_process_location_piece_postal2(self):
        data = {'postalCode': 'ABC123', 'location': {'address': 'Freedom st. 5'}}
        location_piece = 'postal'
        self.sanitizer.process_location_piece(location_piece, 'postalCode', data)
        self.assertDictEqual({'location': {'address': 'Freedom st. 5, ABC123'}}, data)

    def test_process_location_piece_postal3(self):
        data = {'postalCode': 'ABC123', 'location': {'address': 'Freedom st. 5, ABC123'}}
        location_piece = 'postal'
        self.sanitizer.process_location_piece(location_piece, 'postalCode', data)
        self.assertDictEqual({'location': {'address': 'Freedom st. 5, ABC123'}}, data)

    def test_process_location_piece_address1(self):
        data = {'Address': 'Freedom st. 5'}
        location_piece = 'address'
        self.sanitizer.process_location_piece(location_piece, 'Address', data)
        self.assertDictEqual({'location': {'address': 'Freedom st. 5'}}, data)

    def test_process_location_piece_address2(self):
        data = {'Address': 'Freedom st. 5', 'location': {'address': 'ABC123'}}
        location_piece = 'address'
        self.sanitizer.process_location_piece(location_piece, 'Address', data)
        self.assertDictEqual({'location': {'address': 'Freedom st. 5, ABC123'}}, data)

    def test_process_amenities_helper(self):
        data = {'Facilities': ["Pool", "WiFi ", "Aircon", "BusinessCenter"]}
        key = 'Facilities'
        processed_list = self.sanitizer.process_amenities_helper(key, data)
        self.assertListEqual(["pool", "wifi", "aircon", "business center"], processed_list)

    def test_process_amenities(self):
        data = {'Facilities': ["Pool", "WiFi ", "Aircon", "BusinessCenter"]}
        key = 'Facilities'
        self.sanitizer.process_amenities(key, data)
        self.assertDictEqual({'amenities': {'all': ["pool", "wifi", "aircon", "business center"]}}, data)

    def process_images(self):
        data = {"images": {"rooms": [{"url": "https://1.jpg", "description": "Suite"}, {"url": "https://2.jpg", "description": "Suite - Living room"}], "amenities": [{"url": "https://3.jpg", "description": "Bar"}]}}
        key = 'images'
        self.sanitizer.process_images(key, data)
        self.assertDictEqual({"images": {"rooms": [{"link": "https://1.jpg", "description": "Suite"}, {"link": "https://2.jpg", "description": "Suite - Living room"}], "amenities": [{"link": "https://3.jpg", "description": "Bar"}]}}, data)
